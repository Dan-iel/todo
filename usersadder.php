<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>USERS List</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <h1>USERS List</h1>

    <div class="container">
        <form method="POST">
            <input type="text" name="firstName" placeholder="First Name">
            <input type="text" name="lastName" placeholder="Last Name">
            <input type="submit" value="Ajouter">
        </form>


        <?php
        require 'connexion.php';

        if (!empty($_POST['firstName']) && !empty($_POST['lastName'])) {
            $firstNAME = $_POST['firstName'];
            $lastNAME = $_POST['lastName'];
            $requete = 'INSERT INTO person(firstName, lastName) VALUES ("' . $firstNAME . '", "' . $lastNAME . '")';
            $resultat = $mysqli->query($requete);
            if ($resultat) {
                header("Location:users.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }

        ?>
    </div>
</body>

</html>