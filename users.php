<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODO List</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <h1>Users List</h1>

    <div class="container">
        <table>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th class="operation"></th>
                <th class="operation"></th>
            </tr>

            <?php

            require 'connexion.php';
            $users = $mysqli->query('SELECT * FROM person');

            foreach ($users as $user) { ?>
            <tr>
                <td>
                    <?php echo $user['id'] ?>
                </td>
                <td>
                    <?php echo $user['firstName'] ?>
                </td>
                <td>
                    <?php echo $user['lastName'] ?>
                </td>
                <td>
                    <a class=" btn" href="humaneditor.php?id=<?php echo $user['id']; ?>">Modifier</a>
                    <a class="btn btn-danger" href="delete.php?id=<?php echo $user['id']; ?>"
                        onclick="return confirm('Êtes vous sûr de vouloir supprimer la tâche: <?php echo $user['task']; ?> ?')">Supprimer</a>
                </td>
            </tr>
            <?php } ?>
        </table>

        <p>
            <a href=" ./usersadder.php" class="btn">
                Ajouter une nouvelle user
            </a>
        </p>

    </div>

</body>

</html>